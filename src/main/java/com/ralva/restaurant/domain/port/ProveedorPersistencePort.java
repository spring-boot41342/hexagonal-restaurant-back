package com.ralva.restaurant.domain.port;

import java.util.List;


import com.ralva.restaurant.domain.model.proveedor.Proveedor;

public interface ProveedorPersistencePort {
    Proveedor create(Proveedor request);
    Proveedor getById(Long id);
    List<Proveedor> getAll();
    void deleteById(Long id);
    Proveedor update(Proveedor request);
    List<Proveedor> getByIds(List<Long> tasksIds);
}
