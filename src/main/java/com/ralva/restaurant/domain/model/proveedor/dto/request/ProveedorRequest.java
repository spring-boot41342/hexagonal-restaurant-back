package com.ralva.restaurant.domain.model.proveedor.dto.request;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProveedorRequest {
    private Long id;
    private String nombre;
    private String direccion;
    private String telefono;
    private String email;
    private String usuarioCreacion;
    private Date fechaCreacion;
    private String usuarioModifica;
    private Date fechaModificacion;
}
