package com.ralva.restaurant.domain.model.proveedor.constant;

public class ProveedorConstant {
    public static final String TASK_NOT_FOUND_MESSAGE_ERROR = "No se encontro una tarea con el id %s";
    public static final String CURRENT_TASK_NOT_ALLOW_TO_DELETE = "La tarea %s no se puede eliminar porque tiene usuarios asignados";
}
