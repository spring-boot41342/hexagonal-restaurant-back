package com.ralva.restaurant.domain.model.proveedor.dto.json_placeholder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JsonPlaceDto {
    private Long id;
    private String title;
    private String body;
    private Long userId;
}
