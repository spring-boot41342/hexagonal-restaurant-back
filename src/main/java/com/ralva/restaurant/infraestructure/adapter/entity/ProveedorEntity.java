package com.ralva.restaurant.infraestructure.adapter.entity;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = ProveedorEntity.TABLE_NAME)
public class ProveedorEntity {
    public static final String TABLE_NAME= "PROVEEDOR";
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String nombre;
    private String direccion;
    private String telefono;
    private String email;
    private String usuarioCreacion;
    private Date fechaCreacion;
    private String usuarioModifica;
    private Date fechaModificacion;
}
