package com.ralva.restaurant.infraestructure.adapter.external_service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ralva.restaurant.application.usecases.JsonPlaceholderService;
import com.ralva.restaurant.domain.model.proveedor.dto.json_placeholder.JsonPlaceDto;

@Service
public class JsonPlaceholderServiceExternal implements JsonPlaceholderService{
    private final RestTemplate restTemplate;

    public JsonPlaceholderServiceExternal() {
        restTemplate = new RestTemplate();
    }

    @Override
    public JsonPlaceDto getById(Long id) {
        String apiUrl = "https://jsonplaceholder.typicode.com/posts/" + id;
        ResponseEntity<JsonPlaceDto> response = restTemplate.getForEntity(apiUrl, JsonPlaceDto.class);
        JsonPlaceDto todo = response.getBody();

        return todo;
/*
        if (todo == null) {
            return null;
        }

         apiUrl = "https://jsonplaceholder.typicode.com/users/" + todo.getUserId();
        ResponseEntity<JsonPlaceholderUser> userResponse = restTemplate.getForEntity(apiUrl, JsonPlaceholderUser.class);
        JsonPlaceholderUser user = userResponse.getBody();

        if (user == null) {
            return null;
        } */

        //return new AdditionalTaskInfo(user.getId(), user.getName(), user.getEmail());
    }


}
