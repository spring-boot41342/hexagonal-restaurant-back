package com.ralva.restaurant.infraestructure.adapter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ralva.restaurant.domain.model.proveedor.Proveedor;
import com.ralva.restaurant.domain.model.proveedor.constant.ProveedorConstant;
import com.ralva.restaurant.domain.port.ProveedorPersistencePort;
import com.ralva.restaurant.infraestructure.adapter.exception.ProveedorException;
import com.ralva.restaurant.infraestructure.adapter.mapper.ProveedorDboMapper;
import com.ralva.restaurant.infraestructure.adapter.repository.ProveedorRepository;

@Service
public class ProveedorSpringJpaAdapter implements ProveedorPersistencePort{

    private final ProveedorRepository proveedorRepository;
    private final ProveedorDboMapper proveedorDboMapper;

    public ProveedorSpringJpaAdapter(ProveedorRepository proveedorRepository, ProveedorDboMapper proveedorDboMapper) {
        this.proveedorRepository = proveedorRepository;
        this.proveedorDboMapper = proveedorDboMapper;
    }

    @Override
    public Proveedor create(Proveedor request) {
        var taskToSave = proveedorDboMapper.toDbo(request);
        var taskSaved = proveedorRepository.save(taskToSave);

        return proveedorDboMapper.toDomain(taskSaved);
    }

    @Override
    public Proveedor getById(Long id) {
        var optionalTask = proveedorRepository.findById(id);

        if (optionalTask.isEmpty()){
            throw new ProveedorException(HttpStatus.NOT_FOUND, String.format(ProveedorConstant.TASK_NOT_FOUND_MESSAGE_ERROR, id));
        }

        return proveedorDboMapper.toDomain(optionalTask.get());
    }

    @Override
    public List<Proveedor> getAll() {
        return proveedorRepository.findAll()
                .stream()
                .map(proveedorDboMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        proveedorRepository.deleteById(id);
    }

    @Override
    public Proveedor update(Proveedor request) {
        var taskToUpdate = proveedorDboMapper.toDbo(request);
        var taskUpdated = proveedorRepository.save(taskToUpdate);

        return proveedorDboMapper.toDomain(taskUpdated);
    }

    @Override
    public List<Proveedor> getByIds(List<Long> tasksIds) {
        return proveedorRepository.findByIdIn(tasksIds)
        .stream()
        .map(proveedorDboMapper::toDomain)
        .collect(Collectors.toList());
    }
    
}
