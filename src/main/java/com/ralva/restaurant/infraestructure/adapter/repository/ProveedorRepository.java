package com.ralva.restaurant.infraestructure.adapter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ralva.restaurant.infraestructure.adapter.entity.ProveedorEntity;

@Repository
public interface ProveedorRepository extends JpaRepository<ProveedorEntity, Long> {
    List<ProveedorEntity> findByIdIn(List<Long> tasksIds);
}
