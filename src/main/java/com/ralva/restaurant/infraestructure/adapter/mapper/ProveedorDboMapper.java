package com.ralva.restaurant.infraestructure.adapter.mapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.ralva.restaurant.domain.model.proveedor.Proveedor;
import com.ralva.restaurant.infraestructure.adapter.entity.ProveedorEntity;

@Mapper(componentModel = "spring")
public interface ProveedorDboMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "nombre", target = "nombre")
    @Mapping(source = "direccion", target = "direccion")
    @Mapping(source = "telefono", target = "telefono")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "usuarioCreacion", target = "usuarioCreacion")
    @Mapping(source = "fechaCreacion", target = "fechaCreacion")
    @Mapping(source = "usuarioModifica", target = "usuarioModifica")
    @Mapping(source = "fechaModificacion", target = "fechaModificacion")
    ProveedorEntity toDbo(Proveedor domain);

    @InheritInverseConfiguration
    Proveedor toDomain(ProveedorEntity entity);
}
