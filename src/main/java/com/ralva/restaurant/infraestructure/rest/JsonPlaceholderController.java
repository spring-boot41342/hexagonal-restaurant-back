package com.ralva.restaurant.infraestructure.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ralva.restaurant.application.usecases.JsonPlaceholderService;
import com.ralva.restaurant.domain.model.proveedor.dto.json_placeholder.JsonPlaceDto;

@RestController
@RequestMapping("/jsonplaceholder")
public class JsonPlaceholderController {

    private final JsonPlaceholderService jsonPlaceholderService;

    public JsonPlaceholderController(JsonPlaceholderService jsonPlaceholderService) {
        this.jsonPlaceholderService = jsonPlaceholderService;
    }

    @GetMapping("/{id}")
    public JsonPlaceDto getById(@PathVariable Long id){
        return jsonPlaceholderService.getById(id);
    }
}
