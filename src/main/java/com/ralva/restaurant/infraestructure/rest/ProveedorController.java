package com.ralva.restaurant.infraestructure.rest;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ralva.restaurant.application.usecases.ProveedorService;
import com.ralva.restaurant.domain.model.proveedor.dto.ProveedorDto;
import com.ralva.restaurant.domain.model.proveedor.dto.request.ProveedorRequest;

@RestController
@RequestMapping("/proveedor")
public class ProveedorController {
    private final ProveedorService proveedorService;

    public ProveedorController(ProveedorService proveedorService) {
        this.proveedorService = proveedorService;
    }

    @GetMapping("/{id}")
    public ProveedorDto getById(@PathVariable Long id){
        return proveedorService.getById(id);
    }

    @GetMapping
    public List<ProveedorDto> getAll() {
        return proveedorService.getAll();
    }

    @PostMapping()
    public ProveedorDto create(@RequestBody ProveedorRequest proveedorRequest){
        return proveedorService.createNew(proveedorRequest);
    }

    @PutMapping("/{id}")
    public ProveedorDto edit(@RequestBody ProveedorRequest proveedorRequest,
                               @PathVariable Long id){
        return proveedorService.update(proveedorRequest, id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id){
        proveedorService.deleteById(id);
    }
}
