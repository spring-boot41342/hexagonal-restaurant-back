package com.ralva.restaurant.application.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ralva.restaurant.application.mapper.ProveedorDtoMapper;
import com.ralva.restaurant.application.mapper.ProveedorRequestMapper;
import com.ralva.restaurant.application.usecases.ProveedorService;
import com.ralva.restaurant.domain.model.proveedor.constant.ProveedorConstant;
import com.ralva.restaurant.domain.model.proveedor.dto.ProveedorDto;
import com.ralva.restaurant.domain.model.proveedor.dto.request.ProveedorRequest;
import com.ralva.restaurant.domain.port.ProveedorPersistencePort;
import com.ralva.restaurant.infraestructure.adapter.exception.UserException;

@Service
public class ProveedorManagamentService implements ProveedorService {

    private final ProveedorPersistencePort proveedorPersistencePort;
    private final ProveedorRequestMapper proveedorRequestMapper;
    private final ProveedorDtoMapper proveedorDtoMapper;

    public ProveedorManagamentService(ProveedorPersistencePort proveedorPersistencePort,
            ProveedorRequestMapper proveedorRequestMapper, ProveedorDtoMapper proveedorDtoMapper) {
        this.proveedorPersistencePort = proveedorPersistencePort;
        this.proveedorRequestMapper = proveedorRequestMapper;
        this.proveedorDtoMapper = proveedorDtoMapper;
    }

    @Override
    public ProveedorDto createNew(ProveedorRequest request) {
        var taskToCreate = proveedorRequestMapper.toDomain(request);

        var taskCreated = proveedorPersistencePort.create(taskToCreate);

        return proveedorDtoMapper.toDto(taskCreated);
    }

    @Override
    public ProveedorDto getById(Long id) {
        var task = proveedorPersistencePort.getById(id);

        return proveedorDtoMapper.toDto(task);
    }

    @Override
    public List<ProveedorDto> getAll() {
       var tasks = proveedorPersistencePort.getAll();
        return tasks
                .stream()
                .map(proveedorDtoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        var task = proveedorPersistencePort.getById(id);

        if(!task.getNombre().isEmpty()){
            throw new UserException(HttpStatus.BAD_REQUEST,
                    String.format(ProveedorConstant.CURRENT_TASK_NOT_ALLOW_TO_DELETE, task.getId()));
        }
        proveedorPersistencePort.deleteById(id);
    }

    @Override
    public ProveedorDto update(ProveedorRequest request, Long id) {
        var taskToUpdate = proveedorRequestMapper.toDomain(request);

        taskToUpdate.setNombre(request.getNombre());
        taskToUpdate.setDireccion(request.getDireccion());
        //taskToUpdate.setTimeRequiredToComplete(request.getTimeRequiredToComplete());

        var taskUpdated = proveedorPersistencePort.update(taskToUpdate);

        return proveedorDtoMapper.toDto(taskUpdated);
    }
    
}
