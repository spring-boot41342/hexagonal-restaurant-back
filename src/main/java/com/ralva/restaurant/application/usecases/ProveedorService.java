package com.ralva.restaurant.application.usecases;

import java.util.List;

import com.ralva.restaurant.domain.model.proveedor.dto.ProveedorDto;
import com.ralva.restaurant.domain.model.proveedor.dto.request.ProveedorRequest;

public interface ProveedorService {
    ProveedorDto createNew(ProveedorRequest request);
    ProveedorDto getById(Long id);
    List<ProveedorDto> getAll();
    void deleteById(Long id);
    ProveedorDto update(ProveedorRequest request, Long id);
}
