package com.ralva.restaurant.application.usecases;

import com.ralva.restaurant.domain.model.proveedor.dto.json_placeholder.JsonPlaceDto;

public interface JsonPlaceholderService {
    JsonPlaceDto getById(Long id);
}
