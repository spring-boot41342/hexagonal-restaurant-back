package com.ralva.restaurant.application.mapper;

import com.ralva.restaurant.domain.model.proveedor.Proveedor;
import com.ralva.restaurant.domain.model.proveedor.dto.request.ProveedorRequest;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProveedorRequestMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "nombre", target = "nombre")
    @Mapping(source = "direccion", target = "direccion")
    @Mapping(source = "telefono", target = "telefono")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "usuarioCreacion", target = "usuarioCreacion")
    @Mapping(source = "fechaCreacion", target = "fechaCreacion")
    @Mapping(source = "usuarioModifica", target = "usuarioModifica")
    @Mapping(source = "fechaModificacion", target = "fechaModificacion")
    Proveedor toDomain(ProveedorRequest request);
}
